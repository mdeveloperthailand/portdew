import Layout from '../components/Layout'
import React, { Component } from 'react';



// Section Export Start
export default class About extends Component {
  render() {
    return (
      <Layout>
        <Operation />
      </Layout>
    )
  }
}
// Section Export Close


// Operation Start
class Operation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datastatus: '1',
      name: 'DEW',
      lastname: 'BENEDIX',
    };
    this.switchText = this.switchText.bind(this)
  }

  switchText(){
    this.setState({name: this.state.lastname});
  }

  render() {
    return (
      <div>
        <h1>Test Route About {this.state.name}</h1>
        <button className="gbtn" onClick={this.switchText}>switch</button>
      </div>
    )
  }
}
// Operation Close


