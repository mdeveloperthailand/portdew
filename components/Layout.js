import Header from './Header'
import Footer from './Footer'
import Head from 'next/head';


const Layout = (props) => (
  <div>
    <Head>
      <title>Profile DEW</title>
      <link href="../static/style.css" rel="stylesheet" />
    </Head>
    <Header />
    {props.children}
    <Footer />
  </div>
)

export default Layout