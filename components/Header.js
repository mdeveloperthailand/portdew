import Link from 'next/link'


const Header = () => (
  <div>
    <Link href="/">
      <a className="gbtn">Home</a>
    </Link>
    <Link href="/about">
      <a className="gbtn">About</a>
    </Link>
  </div>
)

export default Header